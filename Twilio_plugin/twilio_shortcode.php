<?php
include_once "twilio-php-master/Twilio/autoload.php";
// Use to Call From Twilio
use Twilio\Jwt\ClientToken;
// Use the REST API Client to make requests to the Twilio REST API
use Twilio\Rest\Client;
add_shortcode( 'TWILIO_CALL', 'twilio_call_shortcode');
function twilio_call_shortcode()
{
     include_once 'account_info.php';
     $capability = new ClientToken($accountSid,$authToken);
     $capability->allowClientOutgoing($appSid);
     $token = $capability->generateToken();
     ?>    
    <script type="text/javascript">
         jQuery(document).ready(function ()
         {
               jQuery(".twilio_phone_call").intlTelInput({     
                    allowDropdown: true,
                    autoHideDialCode: false,
                    autoPlaceholder: true,
                    // dropdownContainer: "body",
                    // excludeCountries: ["us"],
                    // geoIpLookup: function(callback) {
                    //   $.get("http://ipinfo.io", function() {}, "jsonp").always(function(resp) {
                    //     var countryCode = (resp && resp.country) ? resp.country : "";
                    //     callback(countryCode);
                    //   });
                    // },
                    initialCountry: "in",
                    // nationalMode: false,
                    // numberType: "MOBILE",
                    // onlyCountries: ['us', 'gb', 'ch', 'ca', 'do'],
                    // preferredCountries: ['cn', 'jp'],
                    separateDialCode: true,
                    utilsScript: ""
               });
               jQuery("#form_call").find(".selected-dial-code").addClass("call");
               Twilio.Device.setup("<?php echo $token; ?>");

               Twilio.Device.ready(function (device) {
                    jQuery("#log").text("Ready");
               });

               Twilio.Device.error(function (error) {
                    jQuery("#log").text("Error: " + error.message);
               });

               Twilio.Device.connect(function (conn) {
                    jQuery("#log").text("Successfully established call.... ");
                    jQuery("#image_calling").show();
                    jQuery("#image_call").hide();
               });

               Twilio.Device.disconnect(function (conn) {
                    jQuery("#log").text("Call ended");
                    jQuery("#image_calling").hide();
                    jQuery("#image_call").show();
                    
               });
               jQuery("#image_call").on("click",function() {                           
                    jQuery("#form_call").validate({
                              rules: 
                                      {
                                             CallNumber: 
                                             {
                                                  number: true
                                             }
                                      },                             
                              errorPlacement: function(){ return false; },
                              submitHandler: function () 
                              {                            
                                   var full_number=jQuery(".selected-dial-code.call").text()+jQuery("#CallNumber").val();
                                   params = {"CallerId":"<?php echo $twilio_number;  ?>","CallNumber": full_number};
                                   Twilio.Device.connect(params);
                              }
                    });
               
               });
               
               jQuery("#image_hangup").on("click",function(event) {
                    Twilio.Device.disconnectAll();
                      event.preventDefault();
               });               
         });      
    </script>
    <form id="form_call" name="form_call" novalidate="">
         <table>
              <tr>
                   <td>
                        <input type="tel" id="CallNumber" name="CallNumber" class="twilio_phone_call demo_input"  placeholder="Type phone number" required="" >                        
                   </td>
              </tr>
              <tr>
                   <td>
                        <label id="log">Loading..... </label>
                   </td>
              </tr>
              <tr>
                   <td>
                        <input type="image"  border="0" id="image_call"  src="<?php echo TWILIO_PLUGIN_URL; ?>img/call.jpg" height="50px" width="50px" title="Call" alt="Submit" />
                        <input type="image" border="0" id="image_calling" src="<?php echo TWILIO_PLUGIN_URL; ?>img/calling.jpg" height="50px" width="50px" title="Calling" hidden="true" />
                        <input type="image" border="0" id="image_hangup" src="<?php echo TWILIO_PLUGIN_URL; ?>img/hangup.jpg" height="50px" width="50px" title="hangup" >                       
                   </td>
              </tr>
         </table>
  </form>       
<?php
}
add_shortcode( 'TWILIO_SMS', 'twilio_sms_shortcode' );
function twilio_sms_shortcode()
{     
     ?>    
     <script type="text/javascript">
         jQuery(document).ready(function ()
         {
              jQuery(".twilio_phone_sms").intlTelInput({     
                   allowDropdown: true,
                    autoHideDialCode: false,
                    autoPlaceholder: true,
                    // dropdownContainer: "body",
                    // excludeCountries: ["us"],
                    // geoIpLookup: function(callback) {
                    //   $.get("http://ipinfo.io", function() {}, "jsonp").always(function(resp) {
                    //     var countryCode = (resp && resp.country) ? resp.country : "";
                    //     callback(countryCode);
                    //   });
                    // },
                    initialCountry: "in",
                    // nationalMode: false,
                    // numberType: "MOBILE",
                    // onlyCountries: ['us', 'gb', 'ch', 'ca', 'do'],
                    // preferredCountries: ['cn', 'jp'],
                    separateDialCode: true,
                    utilsScript: ""
               });
               jQuery("#image_send_sms").on("click",function (){
                    jQuery("#form_sms").validate({
                         rules: 
                                 {
                                      SmsNumber: {number: true}
                                  },                         
                         errorPlacement: function(){ return false; },
                         submitHandler: function () 
                         {
                              twilio_send_sms();
                         }
                    });
               });               
               jQuery("#form_sms").find(".selected-dial-code").addClass("sms");
         });
     </script>
     <form id="form_sms" name="form_sms" novalidate="">              
         <table>
              <tr>
                   <td>
                        <input  type="tel" id="SmsNumber" name="SmsNumber" class="twilio_phone_sms demo_input" placeholder="Type phone number" required="" >       
                   </td>
              </tr>
              <tr>
                   <td>
                        <textarea id="textarea_sms" rows="3" cols="28"  name="textarea_sms" placeholder="Type your message"  required=""></textarea>       
                   </td>
              </tr>
              <tr>
                   <td>
                        <input type="image" title="send sms"  border="1" id="image_send_sms" name="image_send_sms" src="<?php echo TWILIO_PLUGIN_URL; ?>img/send_sms.png" 
                               class="send_sms"   height="50px" width="50px"   />
                        <img id="sms_loader"  src="http://www.anastasiabeverlyhills.com/skin/frontend/mtghost/default/images/loader.gif" hidden="true">
                        <img id="sms_sending" src="https://cdn0.iconfinder.com/data/icons/simple-seo-and-internet-icons/512/sending_e-mail_envelope_marketing-512.png" height="50px" width="50px" hidden="true" >
                         <span id="span_sms"></span>
                   </td>
              </tr>              
         </table>
     </form>
    <?php
}
function twilio_sms()
{     
     include_once 'account_info.php';
     // Your Account SID and Auth Token from twilio.com/console
     $client = new Client($accountSid, $authToken);
     $receiver=$_REQUEST["receiver"];
     $message=$_REQUEST["message"];
     try
     {
//           Use the client to do fun stuff like send text messages!
          $client->messages->create
          (   // the number you'd like to send the message to
               $receiver,array('from' => $twilio_number,'body' => $message)
          );
          echo " Sms Sent <img  src=' ".TWILIO_PLUGIN_URL."img/sent_email.ico'  height='50' width='50'  >";          
     } 
     catch (Exception $ex) 
     {
          echo "Sms Not Sent<img  src='https://cdn3.iconfinder.com/data/icons/flatastic-10-2/256/email-not-validated-128.png'  height='50' width='50' >";
     }
     exit();
}
add_action("wp_ajax_twilio_sms","twilio_sms");