<?php
     /* 
      * Plugin Name: Twilio
       *Description : Twilio plugin for SMS and OUTGOING CALL functionality 
      * Author : Nikunj Sharma
      */
if (!defined('ABSPATH')) 
{
    exit; // Exit if accessed directly
}
define('TWILIO_PLUGIN_URL', plugin_dir_url(__FILE__));
define('TWILIO_PLUGIN_PATH', plugin_dir_path(__FILE__));
if(!class_exists("Twilio"))
{
     class Twilio
     {
          public function __construct()
          {
               //create admin settings page hook
               add_action('admin_menu', array($this, 'twilio_settings_page'));               
               //register stylesheets
               add_action("admin_enqueue_scripts",array($this,"enqueue_twilio_style"));
               add_action("wp_enqueue_scripts",array($this,"enqueue_twilio_style"));                              
               //register scripts
               add_action("admin_enqueue_scripts",array($this,"enqueue_twilio_script"));
               add_action("wp_enqueue_scripts",array($this,"enqueue_twilio_script"));               
               //include shortcode file
               include_once 'twilio_shortcode.php';               
          }
          /**
         * settings page
         */
          public function twilio_settings_page()
          {
               add_menu_page('Twilio', 'Twilio', 'manage_options', 'twilio', array($this, 'twilio_settings_page_function'),'dashicons-phone');
          }
          public function twilio_settings_page_function()
          {
 ?>
               <div class="twilio_settings" style="margin-top: 20px;">
                    <img src="<?php echo TWILIO_PLUGIN_URL; ?>img/twilio.png"/>
                    <br>
                    <table class="update-nag">
                         <tbody>
                              <tr>
                                   <td><b>Call Short Code: </b> [TWILIO_CALL]</td>
                              </tr>
                              <tr>
                                   <td><b>Sms Short Code: </b> [TWILIO_SMS]</td>
                              </tr>
                         </tbody>
                    </table>
                    <hr/>
                    <h3>Settings</h3>
<?php
                    //save settings
                    if (isset($_POST['setting_twilio_number']))
                    {
                         update_option('tw_setting_twilio_number',$_POST['setting_twilio_number']);
                         update_option('tw_setting_twilio_account_sid',$_POST['setting_twilio_account_sid']);
                         update_option('tw_setting_twilio_auth_token',$_POST['setting_twilio_auth_token']);
                         update_option('tw_setting_twilio_twiml_appid',$_POST['setting_twilio_twiml_appid']);
                         ?>
                         <div class="updated below-h2" id="message"><p>Settings updated successfully.</p></div>
<?php
               }
?>
                    <form method="post" class="form-table">    
                              <table>
                              <tbody>
                                   <tr>
                                        <td><strong>Twilio Account SID</strong></td>
                                        <td>
                                             <input value="<?php echo get_option('tw_setting_twilio_account_sid'); ?>" name="setting_twilio_account_sid" type="text" class="demo_input"/>
                                             <p class="description">Your Account SID you can get it from: <a target="_blank" href='https://www.twilio.com/user/account/'>https://www.twilio.com/user/account/</a></p>
                                        </td>
                                   </tr>
                                   <tr>
                                        <td><strong>Twilio Auth Token</strong></td>
                                        <td>
                                             <input value="<?php echo get_option('tw_setting_twilio_auth_token'); ?>" name="setting_twilio_auth_token" type="text" class="demo_input"/>
                                             <p class="description">Your Account Auth Token you can get it from: <a target="_blank" href='https://www.twilio.com/user/account/'>https://www.twilio.com/user/account/</a></p>
                                        </td>
                                   </tr>   
                                   <tr>
                                        <td><strong>Twilio Number</strong></td>
                                        <td>
                                             <input value="<?php echo get_option('tw_setting_twilio_number'); ?>" name="setting_twilio_number" type="text" class="demo_input"/>
                                             <p class="description">Twilio Phone Number (ie. +14695572832)</p>
                                        </td>
                                   </tr>
                                   <tr>
                                        <td><strong>TwiML App Sid (For CALL)</strong></td>
                                        <td>
                                             <input value="<?php echo get_option('tw_setting_twilio_twiml_appid'); ?>" name="setting_twilio_twiml_appid" type="text" class="demo_input" />
                                             <p class="description">TwiML app sid for call</p>
                                        </td>
                                   </tr>
                              </tbody>
                         </table>       
                         <p class="submit">
                              <input type="submit" value="Save Changes" class="button button-primary demo_button">
                         </p>    
                    </form>
                    <hr/>
               </div>
<?php
          }
          public function enqueue_twilio_style()
          {
               wp_enqueue_style('twilio-intlTelInput', TWILIO_PLUGIN_URL . 'build/css/intlTelInput.css');
               wp_enqueue_style('twilio-demo', TWILIO_PLUGIN_URL . 'build/css/demo.css');
          }
          public function enqueue_twilio_script()
          {            
               wp_enqueue_script('twilio-intTelInput', TWILIO_PLUGIN_URL . 'build/js/intlTelInput.js',array( 'jquery' ));
?>
<script>
     var ajax_url="<?php echo admin_url("admin-ajax.php"); ?>";
</script>
<?php
               wp_enqueue_script('phone_number', TWILIO_PLUGIN_URL . 'js/twilio_send_sms.js',array( 'jquery' ));
               wp_enqueue_script('jquery_validate', TWILIO_PLUGIN_URL . 'js/jquery.validate.min.js',array( 'jquery' ));
               wp_enqueue_script("twilio-min","https://media.twiliocdn.com/sdk/js/client/v1.3/twilio.min.js",array( 'jquery' ));   
          }    
     }
     $twilio = new Twilio();
}