jQuery(document).ready(function ()
{
     twilio_send_sms = function ()
     {
          var full_number = jQuery(".selected-dial-code.sms").text() + jQuery("#SmsNumber").val();
          var message = jQuery("#textarea_sms").val();
          jQuery("#sms_loader,#sms_sending").show();
          jQuery.ajax(
                  {
                       type: 'POST',
                       url: ajax_url,
                       data: {receiver: full_number, message: message, action: "twilio_sms"},
                       success: function (data, textStatus, jqXHR)
                       {
                            setTimeout(function ()
                            {
                                 jQuery("#sms_loader,#sms_sending").hide();
                            }, 3000);
                       }
                  }).done(
                  function (data)
                  {
                       setTimeout(function ()
                       {
                            jQuery("#span_sms").html(data);
                            jQuery("#span_sms").fadeIn("slow");
                       }, 3000);
                       setTimeout(function ()
                       {
                            jQuery("#span_sms").fadeOut("slow");
                       }, 5000);
                  }
          );
     };
});